.PHONY: all router client

BUILD_DIR=build
BOARD=nrf52840dk_nrf52840
CLIENT_SNR=683933481
ROUTER_SNR=683594269

all: router client

router:
	west build -b $(BOARD) -d $(BUILD_DIR)/router router

client:
	west build -b $(BOARD) -d $(BUILD_DIR)/client client

flash_router: router
	west flash -d $(BUILD_DIR)/router --snr $(ROUTER_SNR)

flash_client: client
	west flash -d $(BUILD_DIR)/client --snr $(CLIENT_SNR)

clean:
	rm -rf $(BUILD_DIR)
