# OpenThread Power Analysis

This repo holds the code used to analyse power usage of OpenThread in a battery powered IoT setting.

## How to clone this repo

To clone run the following command:

```bash
west init -m git@gitlab.com:oskaro/openthread-power-analysis.git openthread-power-analysis
west update
pip install -r zephyr/scripts/requirements.txt
pip install -r nrf/scripts/requirements.txt
pip install -r bootloader/mcuboot/scripts/requirements.txt
source zephyr/zephyr-env.sh
cd app
make
```

This requires the GNU toolchain to be installed and the toolchain variables to be exported in `~/.zephyrrc` as follows:

```bash
export ZEPHYR_TOOLCHAIN_VARIANT=gnuarmemb
export GNUARMEMB_TOOLCHAIN_PATH="/path/to/toolchain"
```


## Structure of the repo

For this analysis I have copied the openthread/coap_server and openthread/coap_client examples from the nRF Connect SDK.
There is one folder for the router code and one for the client. As of writing this README I see that the naming is stupid and should be changed, but I cannot
be bothered to change it by now.

There is a Makefile set up to build and flash the two projects to their respective DevKits, this repo is not created to be used by anyone else than me.

